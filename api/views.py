from django.views import View
from django.http.response import JsonResponse, HttpResponseBadRequest
from django.http.response import HttpResponseNotFound, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.http import QueryDict
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from django.core.exceptions import ObjectDoesNotExist
import json
from django.utils import timezone
from . import models
from . import serializers
from .validators import JobValidator, UserValidator


class Register(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(Register, self).dispatch(*args, **kwargs)

    def post(self, request):
        if 'username' in request.POST \
         and 'password' in request.POST \
         and 'email' in request.POST \
         and 'first_name' in request.POST \
         and 'last_name' in request.POST:
            email = request.POST['email']
            if self.is_email_duplicate(email):
                return self.send_duplicate_error('email')
            validator = UserValidator(request.POST)
            if validator.is_valid():
                user = validator.save(commit=False)
                password = request.POST['password']
                user.set_password(password)
                user.save()
                token = get_token_or_expire(user)
                serializer = serializers.TokenSerializer(token)
                return JsonResponse(serializer.data, safe=False)
            return bad_request_error(validator.errors)
        else:
            data = {
                "error": True,
                "message": "please send all needed data"
            }
            js = json.dumps(data)
            return HttpResponseBadRequest(js, content_type='application/json')

    def is_username_duplicate(self, username):
        if User.objects.filter(username=username).exists():
            return True
        else:
            return False

    def is_email_duplicate(self, email):
        if User.objects.filter(email=email).exists():
            return True
        else:
            return False

    def send_duplicate_error(self, duplicate):
        data = {
            "error": True,
            "message": "duplicate {}".format(duplicate),
        }
        js = json.dumps(data)
        return HttpResponse(
            js,
            content_type='application/json',
            status=409
        )


class Login(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(Login, self).dispatch(*args, **kwargs)

    def post(self, request):
        if 'username' in request.POST and 'password' in request.POST:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                token = get_token_or_expire(user)
                serializer = serializers.TokenSerializer(token)
                return JsonResponse(serializer.data, safe=False)
            else:
                data = {
                    "error": True,
                    "message": "invalid credential"
                }
                js = json.dumps(data)
                return HttpResponseNotFound(
                    js, content_type='application/json')
        else:
            data = {
                "error": True,
                "message": "please enter username and password"
            }
            js = json.dumps(data)
            return HttpResponseBadRequest(js, content_type='application/json')


class ListCreateScore(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ListCreateScore, self).dispatch(*args, **kwargs)

    def get(self, request):
        if not hasattr(request, 'user'):
            return send_unauthorized()
        board = models.Leaderboard.objects.all()
        pagination = request.GET.get('pagination', '')
        if len(pagination) == 0:
            serializer = serializers.LeaderboardSerializer(board, many=True)
            return JsonResponse(serializer.data, safe=False)
        else:
            page = request.GET.get('page', 1)
            return self.paginate_board(page, board)

    def paginate_board(self, page, board):
        paginator = Paginator(board, 10)
        try:
            paginate_board = paginator.page(page)
        except PageNotAnInteger:
            paginate_board = paginator.page(1)
        except EmptyPage:
            paginate_board = paginator.page(paginator.num_pages)
        serializer = serializers.LeaderboardSerializer(
            paginate_board, many=True)
        data = {
            'number_of_pages': paginator.num_pages,
            'current': page,
            'data': serializer.data,
        }
        return JsonResponse(data, safe=False)


class ListCreateJob(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ListCreateJob, self).dispatch(*args, **kwargs)

    def get(self, request):
        if not hasattr(request, 'user'):
            return send_unauthorized()
        jobs = models.Job.objects.all()
        pagination = request.GET.get('q', '')
        if len(pagination) == 0:
            serializer = serializers.JobSerializer(jobs, many=True)
            return JsonResponse(serializer.data, safe=False)
        else:
            page = request.GET.get('page', 1)
            return self.paginate_jobs(page, jobs)

    def paginate_jobs(self, page, jobs):
        paginator = Paginator(jobs, 10)
        try:
            paginate_jobs = paginator.page(page)
        except PageNotAnInteger:
            paginate_jobs = paginator.page(1)
        except EmptyPage:
            paginate_jobs = paginator.page(paginator.num_pages)
        serializer = serializers.JobSerializer(
            paginate_jobs, many=True)
        data = {
            'number_of_pages': paginator.num_pages,
            'current': page,
            'jobs': serializer.data,
        }
        return JsonResponse(data, safe=False)

    def post(self, request):
        if not hasattr(request, 'user'):
            return send_unauthorized()
        data = get_request_dict(request)
        data.update({'user': request.user.id})
        validator = JobValidator(data)
        if not validator.is_valid():
            return bad_request_error(validator.errors)
        job = validator.save()
        serializer = serializers.JobSerializer(job)
        return JsonResponse(serializer.data, safe=False)


class RetrieveUpdateDestroyJob(View):

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(RetrieveUpdateDestroyJob, self).dispatch(*args, **kwargs)

    def get(self, request, pk):
        if not hasattr(request, 'user'):
            return send_unauthorized()
        job = get_job(pk)
        if isinstance(job, HttpResponse):
            return job
        serializer = serializers.JobSerializer(job)
        return JsonResponse(serializer.data, safe=False)

    def delete(self, request, pk):
        if not hasattr(request, 'user'):
            return send_unauthorized()
        user_id = request.user.id
        job = get_job(pk)
        if isinstance(job, HttpResponse):
            return job
        if not job.user.id == user_id:
            return permission_error()
        job.delete()
        return HttpResponse('', status=204, content_type='application/json')

    def put(self, request, pk):
        if not hasattr(request, 'user'):
            return send_unauthorized()
        job = get_job(pk)
        if isinstance(job, HttpResponse):
            return job
        if not job.user.id == user_id:
            return permission_error()
        validator = JobValidator(get_request_dict(request), instance=job)
        if not validator.is_valid():
            return bad_request_error(validator.errors)
        validator.save()
        serializer = serializers.JobSerializer(job)
        return JsonResponse(serializer.data, safe=False)


def permission_error():
    data = {
        "error": True,
        "message": "permission error!"
    }
    js = json.dumps(data)
    return HttpResponse(js, content_type='application/json', status=401)


def send_unauthorized():
    data = {
        "error": True,
        "message": "unauthorized!"
    }
    js = json.dumps(data)
    return HttpResponse(js, content_type='application/json', status=401)


def get_token_or_expire(user):
    if models.Token.objects.filter(user=user).exists():
        token = models.Token.objects.get(user=user)
        if token.expire <= timezone.now():
            token.delete()
            token = create_new_token(user)
    else:
        token = create_new_token(user)
    return token


def create_new_token(user):
    token = models.Token()
    token.user = user
    token.token = get_random_string(length=200)
    token.save()
    return token


def bad_request_error(errors):
    data = {}
    for error in errors:
        data.update({'{}'.format(error): 'please fill {}'.format(error)})
    js = json.dumps(data)
    return HttpResponseBadRequest(content=js, content_type='application/json')


def get_job(pk):
    try:
        job = models.Job.objects.get(pk=pk)
    except models.Job.DoesNotExist:
        data = {
            "error": True,
            "message": "not found",
        }
        js = json.dumps(data)
        return HttpResponseNotFound(js, content_type='application/json')
    return job


def get_request_dict(request):
    qd = QueryDict(request.body)
    put_dict = {k: v[0] if len(v) == 1 else v for k, v in qd.lists()}
    return put_dict
