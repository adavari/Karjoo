from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^jobs/$', views.ListCreateJob.as_view(), name='JobsList'),
    url(r'^jobs/(?P<pk>\d+)/$', views.RetrieveUpdateDestroyJob.as_view(),
        name='Job_Detail'),
    url(r'^login/$', views.Login.as_view(), name='login'),
    url(r'^register/$', views.Register.as_view(), name='register'),
    url(r'^leaderboard/$', views.ListCreateScore.as_view(), name='Leaderboard'),
]
