from .models import Job
import json
import serpy


class UserSerializer(serpy.Serializer):
    id = serpy.IntField()
    username = serpy.StrField()
    email = serpy.StrField()
    first_name = serpy.StrField()
    last_name = serpy.StrField()


class LeaderboardSerializer(serpy.Serializer):
    player = serpy.StrField()
    score = serpy.IntField()
    updated = serpy.StrField()


class TokenSerializer(serpy.Serializer):
    token = serpy.StrField()
    expire = serpy.StrField()


class TagSerializer(serpy.Serializer):
    word = serpy.StrField()


class JobSerializer(serpy.Serializer):
    id = serpy.IntField()
    title = serpy.StrField(required=True)
    description = serpy.StrField()
    phone = serpy.StrField()
    email = serpy.StrField()
    link = serpy.StrField()
    is_external = serpy.StrField()
    like = serpy.IntField()
    dislike = serpy.IntField()
    tags = TagSerializer(many=True, attr="tags.all", call=True)
    created_at = serpy.StrField()
    updated_at = serpy.StrField()
