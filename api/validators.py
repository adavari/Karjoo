from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import Job, Tag


class UserValidator(ModelForm):
    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'email',
            'first_name',
            'last_name',
        ]


class TagValidator(ModelForm):
    class Meta:
        model = Tag
        fields = [
            'word'
        ]


class JobValidator(ModelForm):

    class Meta:
        model = Job
        fields = [
            'title',
            'description',
            'phone',
            'email',
            'link',
            'is_external',
            'like',
            'dislike',
            'created_at',
            'user'
        ]
