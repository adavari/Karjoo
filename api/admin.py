from django.contrib import admin
from .models import Job, Tag, Token, Leaderboard
# Register your models here.


admin.site.register(Job)
admin.site.register(Tag)
admin.site.register(Token)
admin.site.register(Leaderboard)
