from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from datetime import timedelta


class Token(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=200)
    expire = models.DateTimeField(
        default=timezone.now() + timedelta(days=20), blank=True)

    def __str__(self):
        return self.token


class Leaderboard(models.Model):
    player = models.ForeignKey(User)
    player_name = models.CharField(max_length=30)
    score = models.IntegerField()
    updated = models.DateTimeField(auto_now_add=True)

    def ranking(self):
        rs = Leaderboard.objects.filter(score__gt=self.score)
        return rs.aggregate(ranking=Count('score'))['ranking'] + 1


class Tag(models.Model):
    word = models.CharField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.word


class Job(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True, default='')
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    phone = models.CharField(max_length=11, blank=True)
    email = models.EmailField(blank=True)
    link = models.URLField(blank=True)
    is_external = models.CharField(max_length=30, blank=True)
    like = models.IntegerField(default=0, blank=True)
    dislike = models.IntegerField(default=0, blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    tags = models.ManyToManyField(Tag, related_name='jobs', blank=True)

    def __str__(self):
        return '{}_{}'.format(self.title, self.created_at)
