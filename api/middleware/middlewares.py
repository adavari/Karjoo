from django.contrib.auth.models import User
from api.models import Token
from django.contrib import auth
import json
from django.http import HttpResponse
from django.http.response import JsonResponse
from django.contrib import auth
from django.utils.functional import SimpleLazyObject
from django.utils.deprecation import MiddlewareMixin


def get_user(request):
    if not hasattr(request, '_cached_user'):
        request._cached_user = auth.get_user(request)
    return request._cached_user


class TokenAuthenticationMiddleware(MiddlewareMixin):

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        if 'HTTP_COOKIE' in request.META:
            print('csrf')
            request.user = SimpleLazyObject(lambda: get_user(request))
            # print(user.username)
            # if user.username == ' ':
            #     print('if')
            #      = user
            response = self.get_response(request)
        else:
            print('token')
            r = self.process_request(request)
            if isinstance(r, HttpResponse):
                response = r
            else:
                response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def process_request(self, request):
        if 'HTTP_AUTHORIZATION' in request.META:
            token = self.get_token_from_header(
                request.META['HTTP_AUTHORIZATION'])
            token = self.is_authenticated(token)
            if token is not None:
                user = User.objects.get(pk=token.user.id)
                request.user = user
                # auth.login(request, user)
        #     else:
        #         return self.send_unauthorized()
        # else:
        #     return self.send_unauthorized()

    def get_token_from_header(self, header):
        return header[7:]

    def is_authenticated(self, token):
        if Token.objects.filter(token=token).exists():
            return Token.objects.get(token=token)
        else:
            return None

    def send_unauthorized(self):
        data = {
            "error": True,
            "message": "unauthorized!"
        }
        js = json.dumps(data)
        return HttpResponse(js, content_type='application/json', status=401)
